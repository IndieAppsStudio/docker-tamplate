# Docker Tamplate
Berikut cara penggunaan tamplate ini
1.  Clone ke file /opt/
2.  jalankan perintah `mv docker-tamplate containers`
3.  jalankan perintah `apt install apache2-utils` 
4.  jalankan perintah berikut pada terminal jangan lupa ganti yang ada di dalam kurung siku dengan username dan password yang kamu inginkan 
    `echo $(htpasswd -nb <USER> <PASSWORD>) | sed -e s/\\$/\\$\\$/g`
5.  Buka file traefik/docker-compose.yml
6.  Ubah bagian `"traefik.http.middlewares.traefik-auth.basicauth.users=admin:$$apr1$$7Cv/1br7$$CSFTb8U3RkNHCkqW1EA5p."` dengan hasil print perintah di nomor 2
7.  jalankan perintah `chmod 600 /opt/containers/traefik/data/acme.json`
8.  Ubah localhost dengan domain yang kamu pakai
9.  Buka file /traefik/data/trafik.yml
10.  Ubah `email@example.com` email kamu untuk keperluan lets encript
11.  Kembali ke folder /traefik
12.  jalankan perintah `docker network create proxy` untuk membuat network utama
13.  Jalankan perintah `docker-compose up -d`

Untuk file lain kamu hanya perlu menjalankan pont ke-8 dan ke-13

