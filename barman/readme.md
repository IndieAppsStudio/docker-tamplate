# Barman
## How to use
### 1. Setup database server
```bash
docker compose up -d --build postgres
createuser --superuser --replication -P root
createuser --replication -P streaming_barman
sed -i '$ a host   replication    streaming_barman   all md5' /var/lib/postgresql/data/pg_hba.conf
```
### 2. Setup barman server
```bash
docker compose up -d --build barman
docker-compose exec --user=barman barman bash
barman cron
barman check <dbserver [streaming-server]>
barman switch-wal --archive --archive-timeout 60 <dbserver [streaming-server]>
```
### 3. Backup
```bash
barman backup <dbserver [streaming-server]>  --wait
barman list-backup <dbserver [streaming-server]>
```
### 4. Restore
```bash
barman recover <dbserver [streaming-server]> latest /var/lib/postgresql/data
barman recover --get-wal <dbserver [streaming-server]> latest /var/lib/postgresql/data
```

source reverence:
- https://www.enterprisedb.com/docs/supported-open-source/barman/single-server-streaming/step02-backup-setup/
- https://www.scaleway.com/en/docs/tutorials/back-up-postgresql-barman/
