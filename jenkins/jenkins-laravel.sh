docker-compose down

echo "Install Composer"
docker run --rm -v $(pwd):/app -v ${COMPOSER_HOME:-$HOME/.composer}:/tmp composer install

echo "Setup Environtment"
cp .env.example .env
sed -i "s/laravel/${JOB_NAME}/g" .env
sed -i "s/localhost/${APP_DOMAIN}/g" .env
sed -i "s/127.0.0.1/${JOB_NAME}-database/g" .env

echo "Add Directory Permission"
chmod -R a+rw storage
# chmod -R a+rw bootstrap/cache

echo "Build"
docker-compose up -d

docker-compose exec -T --user root application php artisan key:generate
docker-compose exec -T --user root application php artisan storage:link
docker-compose exec -T --user root application php artisan config:cache
docker-compose exec -T --user root application php artisan migrate
echo no | docker-compose exec -T --user root application php artisan passport:install --uuids --force