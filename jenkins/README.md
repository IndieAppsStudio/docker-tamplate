# HOW TO SET ENV FILE
1. run `nano .env`
2. setup env sesuai refrensi berikut [Artikel](https://itnext.io/docker-inside-docker-for-jenkins-d906b7b5f527)
  - run `sudo adduser jenkins`
  - run `id -u jenkins` untuk mengetahui HOST_UID
  - run `getent group | grep docker` untuk mengetahui HOST_GID
  - run `sudo chown jenkins: /var/jenkins_home` untuk memberi permission pada jenkins
3. simpan `ctrl+o`
4. exit `ctrl+x`
5. jalankan jenkins

